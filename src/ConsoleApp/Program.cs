﻿using System;
using UCommerce.EntitiesV2;

namespace ConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
			var orders = PurchaseOrder.All();

			foreach (var order in orders)
			{
				Console.WriteLine(order.Id);
			}

			Console.Read();
		}
	}
}